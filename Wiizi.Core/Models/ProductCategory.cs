﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wiizi.Core.Models
{
    public class ProductCategory : BaseEntity
    {
        public decimal Fator_Conversao { get; set; }
        [DisplayName("Situação")]
        public string Situacao { get; set; }
    }
}
