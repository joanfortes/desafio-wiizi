﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wiizi.Core.Models
{
    public class Product : BaseEntity
    {
        [StringLength(20)]
        [DisplayName("Descrição")]
        public string Descricao { get; set; }
        [DisplayName("Situação")]
        public string Situacao { get; set; }
        [DisplayName("Peso Liquido")]
        public decimal Peso_Liquido { get; set; }
    }
}
