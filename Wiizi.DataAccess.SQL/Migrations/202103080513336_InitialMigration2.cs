namespace Wiizi.DataAccess.SQL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ProductCategories", "Fator_Conversao", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProductCategories", "Fator_Conversao", c => c.String());
        }
    }
}
