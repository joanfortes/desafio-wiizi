namespace Wiizi.DataAccess.SQL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ProductCategories", "Fator_Conversao", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProductCategories", "Fator_Conversao", c => c.Double(nullable: false));
        }
    }
}
