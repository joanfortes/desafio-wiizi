namespace Wiizi.DataAccess.SQL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductCategories", "Fator_Conversao", c => c.String());
            AddColumn("dbo.ProductCategories", "Situacao", c => c.String());
            AddColumn("dbo.ProductCategories", "Unidade", c => c.String());
            DropColumn("dbo.ProductCategories", "Category");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductCategories", "Category", c => c.String());
            DropColumn("dbo.ProductCategories", "Unidade");
            DropColumn("dbo.ProductCategories", "Situacao");
            DropColumn("dbo.ProductCategories", "Fator_Conversao");
        }
    }
}
