﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Wiizi.WebUI.Startup))]
namespace Wiizi.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
